//noinspection ProblematicWhitespace,ProblematicWhitespace
/**
 * Plugin
 *
 * The public functionality will be available on the DOM element the plugin is hooked to.
 * For example, to call the getThing() method from outside the plugin, you:
 *
 * var lol = $('div.lol').lol().get(0);
 * lol.f.getThing();
 *
 * @author Jens Riisom Schultz <jers@fynskemedier.dk>
 */
(function ($) {
    $.fn.PusherPlugin = function (customOptions) {
        var scope = function ($this, publicThis, customOptions) {
            /**
             * Backing field for the Thing property.
             */
            var _value;

            var _pusher;

            /**
             * The default and, after initilization, runtime options.
             * override on object init, ex jQuery('html').adServingPlugin({top_align_to: '.myownclass', etc...});
             */
            var options = {
                url: 'http://localhost',
                port: 3000
            }

            var f = {
                /**
                 * The "constructor"
                 */
                init: function (customOptions) {

                    // Make the public functions publicly available.
                    publicThis.f = $.extend(publicThis.f, f.publicFunctions);

                    // Merge the default options with the given options.
                    $.extend(options, customOptions);

                    $(document).bind('verifyToken', f.actions.verifyTokenKey);

                    $this.load(f.actions.initPusher());

                },

                /**
                 * Public functionality.
                 */
                publicFunctions: {},

                actions: {

                    /* This needs to be begind abstraction, emit local events, not from ws, but handle response from WS. */
                    initPusher: function () {
                        _pusher = io(
                            options.url + ':' + options.port,
                            { query : '?token=token123' }
                        );

                        _pusher.on('reload', function(payload) {
                            f.actions.reload(payload);
                        });
                    },

                    reload : function (payload) {
                        console.log('event reload fired ', payload);
                    }

                },

                installPusher : {
                    requireTokenKey : function () {
                        //remote call to get the token key here, store in cookie.
                    },

                    verifyTokenKey : function (key) {
                        //event postback from backend, with generated key, to match for equal identity.
                    },

                    intallToken : function () {
                        var token = f.actions.requireTokenKey();
                        //put required key in cookie.
                    }
                }
            };

            f.init(customOptions);
        };

        return this.each(function () {
            scope($(this), this, customOptions);
        });
    };
}(jQuery));
