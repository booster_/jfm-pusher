var app = require('express')();
var http = require('http');
var request = require('request');
var server = http.Server(app);
var io = require('socket.io')(server);

server.listen(3000);

/**
 * express middleware for direct http call.
 */
app.use(logger);
app.use(authenticate);

/**
 * express routing
 */
app.get('/', function (request, response) {
    response.send('Restricted access, request has been logged');
    //response.sendFile(__dirname + '/index.html');
});

/**
 * express middleware function
 * @param req Request object
 * @param res Response object
 * @param next Next middleware in the stack
 */
function logger(req, res, next) {
    console.log("logging request\n");
    next();
}

/**
 * express middleware function
 * @param req Request object
 * @param res Response object
 * @param next Next middleware in the stack
 */
function authenticate(req, res, next) {
    console.log("Restricted access, request has been logged\n");
    next();
}

/**
 * socket.io middleware for authenticating user of the websocket.
 */
function middlewareAuth(token) {
    //this is just a test. this hardcoded token, should come from the request call just under here.
    if(token != 'token123') {
        return false;
    }
    console.log('make auth request here, to authenticate user', "\n");
    /*request('http://fyens.dk/modules/web_screen/content/get_data', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var jsonBody = JSON.parse(body);
            console.log(jsonBody.arrArticles[0].headline);
        }
    });*/
}

/**
 * socket.io own middleware use like this:
 *
 * var socket = io('http://localhost:3000', { query: 'token=token123' });
 * remember to call next, if you have multiple middleware functions to call.
 */
io.use(function(socket, next) {
    if(middlewareAuth(socket.handshake.query.token) == false) {
        return false;
    } else {
        console.log("socket.io middleware ", socket.handshake.query.token, "\n");
        next();
    }
});

/* Instance that is spawned on every client connection */
io.on('connection', function (socket) {

    socket.on('connect', function(socket) {
        console.log('connected to server: ', socket);
    });

    socket.on('recievedEvent', function (socket) {
        console.log(socket.message);
    });

    socket.on('eventDispatch', function (message) {
        io.emit('eventDispatch', message);
    });
});