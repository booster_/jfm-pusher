<?php

require 'vendor/autoload.php';

use ElephantIO\Client as Elephant;
use ElephantIO\Engine\SocketIO\Version1X;

$client = new Elephant(new Version1X('http://localhost:3000?token=token123'));
$client->initialize();
$client->emit('eventDispatch', ['channel' => 'politiken_rp', 'event' => 'reload']);
$client->emit('eventDispatch', ['channel' => 'politiken_vp', 'event' => 'reload']);

$client->close();