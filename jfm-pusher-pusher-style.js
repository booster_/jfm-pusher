(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], function () {
            return (root.JfmPusher = factory());
        });
    } else {
        // Browser globals
        root.JfmPusher = factory();
    }
}(this, function () {

    (function () {
        /* main object/constructor */
        function JfmPusher() {
            var self = this;

            this.Channels = new JfmPusher.Channels();
            this.SocketConnection = new JfmPusher.SocketConnection();

            self.connect();

        }

        var prototype = JfmPusher.prototype;

        prototype.reply = function () {
            console.info('hello');
        };

        prototype.getChannels = function () {
            return this.Channels.getChannel();
        };

        prototype.connect = function () {
            this.SocketConnection.connect();
        };

        this.JfmPusher = JfmPusher;

    }).call(this);

    (function() {
        function Channels() {

        }

        var prototype = Channels.prototype;

        prototype.getChannel = function () {
            return new JfmPusher.Channel();
        };

        JfmPusher.Channels = Channels;

    }).call(this);

    (function () {
        function Channel() {

        }

        var prototype = Channel.prototype;

        prototype.getName = function() {
            return 'channel name here';
        };

        JfmPusher.Channel = Channel;

    }).call(this);

    (function () {
        function SocketConnection() {
            this.socket = '';
            this.url = 'http://localhost';
            this.port = 3000;
            this.connected = false;
        }

        var prototype = SocketConnection.prototype;

        prototype.connect = function() {
            this.socket = io(
                this.url + ':' + this.port,
                { query : "token=token123" }
            );

            this.socket.on('connect');

            this.connected = true;
        };

        JfmPusher.SocketConnection = SocketConnection;

    }).call(this);

    return JfmPusher;

}));