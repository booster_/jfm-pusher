/**
 * JfmPusher client
 * Kept in JfmPusher "scope" to not pollute the global
 * scope to much.
 *
 * Constructor function for JfmPusher
 * invoke with the "new" keyword like so
 *
 * "var JfmPusher = new JfmPusher(io)" - Needs instance of socket.io
 * @see http://socket.io/
 *
 * To register a Channel to listen for an giving event, in this case "reload", takes a
 * Closure as a callback, and the event form this client as an argument to that closure:
 *
 *      var fyens_screen = JfmPusher.subscribe('fyens_screen');
 *      fyens_screen.bind('reload', Closure callback);
 *      "fyens_screen.bind('reload', function(event) {
 *          console.log('reload fired');
 *      });"
 *
 * @constructor
 * @version 0.1
 */
var JfmPusher = function (io) {
    /**
     * Channels constructor
     * Channels is responsible for creating the subscribers object map.
     * Each Channel that is being subscribed, will be stored in the object map
     * as an new Channel object and then that Channel object is returned, for further
     * event binding through the method Channel.bind();
     *
     * @constructor
     */
    var Channels = function () {
        this.subscribers = {};
        this.subscribe = function (channelName) {
            if(channelName in this.subscribers) {
                var newChannel = this.subscribers[channelName];
            } else {
                var newChannel = new Channel(channelName);
                this.subscribers[channelName] = newChannel;
            }
            return newChannel;
        };

        /**
         * Method for returning the subscriber object map
         *
         * @returns {{}|*}
         */
        this.getSubscribers = function () {
            return this.subscribers;
        }
    };
    Channels.prototype = Object.create(JfmPusher.prototype);
    Channels.constructor = Channels;


    /**
     * Channel constructor
     *
     * This object will be created on each channel subscribtion, to
     * handle and abstract everything onto a seperate object.
     *
     * document is being used for event bucket. Hense the addEventListener and dispatchEvent on
     * the document, in the fire method on this object.
     *
     * @param channelName
     * @constructor
     */
    var Channel = function (channelName) {
        this.channelName = channelName;
        this.events = {};
        /**
         * Method used for binding Channel and specific event.
         * Same event can be binded, but just not on the same channel :)
         * @param string eventName
         * @param Function eventCallback
         */
        this.bind = function (eventName, eventCallback) {
            /* register event, and callback for later execution when event is fired */
            this.events[eventName] = eventCallback;
        };
        /**
         * This will register and fire the event at once.
         * This method is being called from JfmPusher.dispatchToChannel();
         * Creates an custom event, and fires it with the provided callback as eventListener.
         * @param string eventName
         * @param Function callback
         */
        this.fire = function (eventName, callback, payload) {
            var Event = new CustomEvent(eventName, { 'detail': { payload: payload }, bubbles: true, cancelable: false });
            document.addEventListener(eventName, callback);
            document.dispatchEvent(Event);
            document.removeEventListener(eventName, callback);
        };

    };
    Channel.prototype = Object.create(JfmPusher.prototype);
    Channel.constructor = Channel;

    /*------------------------------------ JfmPusher stuff ------------------------------------*/

    /**
     * Channels object, for subscribing to channels.
     * @type {Channels}
     */
    this.Channels = new Channels();

    /**
     * URL to the websocket
     * @todo supply through argument to JfmPusher constructor function
     * @type {string}
     */
    this.websocketUrl = 'http://localhost';

    /**
     * PORT for the websocket URL
     * @todo supply through argument to JfmPusher constructor function
     * @type {number}
     */
    this.websocketPort = 3000;

    this.websocketQueryString = { query : 'token=token123' };

    /**
     * socket.io instance, JfmPusher is dependends on this!!!
     * @see http://socket.io
     * @type {io}
     */
    var socket = io(this.websocketUrl+':'+this.websocketPort, this.websocketQueryString);

    /**
     * This method is the action method. This will be keeping an ear out
     * for the event "eventDispatch" from the websocket server.
     *
     * And then it will, yes, dispatch the call :)
     */
    this.listenForWebsocket = function() {
        /**
         *  This is the main event dispatcher, getting the event eventDispatch
         *  from the socket.io server on the node_js server, and is going to look at the
         *  payload to see if channel has been subscribed and eventname has been binded.
         */
        socket.on('eventDispatch', function (payload) {
            this.dispatchToChannel(payload);
            this.confirmEvent(socket, payload);
        }.bind(this));

    };

    /**
     * Method for analyzing the response from the websocket server.
     * This method will loop through the subscribed channels, to see if any is to be called.
     * If channel is being called, then see if any event name has been binded, if so invoke it,
     * through the Channel.fire method.
     *
     * @param payload
     */
    this.dispatchToChannel = function (payload) {
        var subscribers = this.Channels.getSubscribers();
        if(payload.channel in subscribers) {

            var subscriber = subscribers[payload.channel];
            var subscriberEvents = subscriber.events;
            if(payload.event in subscriberEvents) {
                subscriber.fire(payload.event, subscriberEvents[payload.event], payload);
            }
        }
    };

    this.confirmEvent = function (socketConnection, payload) {
        console.log(payload);
        socketConnection.emit('recievedEvent', { message: 'event: ' + payload.event + ' recieved and executed on ' + payload.channel});
    };

};

JfmPusher.constructor = JfmPusher;
JfmPusher.prototype.greeting = function (msg) {
    return msg || this.msg;
};